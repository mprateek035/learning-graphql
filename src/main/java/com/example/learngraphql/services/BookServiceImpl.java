package com.example.learngraphql.services;

import com.example.learngraphql.models.db.Book;
import com.example.learngraphql.repo.BookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService{

    @Autowired private BookRepo bookRepo;

    @Override
    public Book create(Book book) {
        return bookRepo.save(book);
    }

    @Override
    public List<Book> getAll() {
        return bookRepo.findAll();
    }

    @Override
    public Book getById(Integer bookId) {
        return bookRepo.findById(bookId).orElse(null);
    }
}
