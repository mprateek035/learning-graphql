package com.example.learngraphql.services;

import com.example.learngraphql.models.db.Book;

import java.util.List;

public interface BookService {
    Book create (Book book);
    List<Book> getAll();
    Book getById(final Integer bookId);
}
