package com.example.learngraphql.repo;

import com.example.learngraphql.models.db.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepo extends JpaRepository<Book, Integer> {
}
