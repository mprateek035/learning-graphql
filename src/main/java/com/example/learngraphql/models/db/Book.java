package com.example.learngraphql.models.db;


import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "graphdb_book")
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String author;
    private String title;
    private String desc;
    private double price;
    private int pages;
}
